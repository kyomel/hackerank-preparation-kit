'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the makeAnagram function below.
function makeAnagram(a, b) {
    let letters = Array.apply(null, new Array(26)).map(Number.prototype.valueOf,0);
    assignWeight(letters,a,1);
    assignWeight(letters,b,-1);

    var solution = letters.reduce(function(prev, curr){
        return Math.abs(prev) + Math.abs(curr);
    });

    return solution;
}

function assignWeight(letters, word, weigth) {
    let charCode;
    for (var i = 0;i < word.length; i++){
        charCode =  word.charCodeAt(i) - 97;
        letters[charCode] += weigth;
    }
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const a = readLine();

    const b = readLine();

    const res = makeAnagram(a, b);

    ws.write(res + '\n');

    ws.end();
}
